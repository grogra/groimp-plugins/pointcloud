# GroIMP point cloud plugin

*This plugin enalbes the management of point cloud in GroIMP following the language XL syntax.*

This project is a plugin for the opensource software [GroIMP](https://grogra.de/) and can only be used from within this platform!
GroIMP is designed as an integrated platform incorporating modelling, visualization and interaction. It exhibits several features that make it suitable for the field of biological and ALife modelling.

The plugin is installed in the default release of GroIMP (>=2.1.5), but for other installations it can be downloaded from the [plugin manager](https://wiki.grogra.de/doku.php?id=user-guide:pluginmanager) or compiled from the [source](#Manual_build).


## Summary

The plugin enables import & export of point cloud from XYZ and PLY file formats.
These point clouds can be added in the simulation as graph, where each point is added as terminal vertices to a balanced tree in the project graph. Adding the objects only as terminal vertices enables to keep the concept of local transformation of GroIMP, which enables to manipulate the points individually using turtle geometry and XL commands.

If the imported file also includes meshes or lines, these objects are added as separate trees. The meshes and lines are then linked to their corresponding points using refinement edges. This structure of separated trees with only specific relationships allows to easily manipulate only one scale of the structure while keeping the included knowledge and relationships.

A more in depth description of the point cloud objects in GroIMP is available on the [software wiki](https://wiki.grogra.de/doku.php?id=groimp-platform:pointcloud).
The current version 1.8 requires groImp 2.1.5 or higher

## Manual build

The manual build require JDK >= 17 and Maven >=3.6. Then run:

```
mvn package
```

This command create a new directory in the parent directory of the plugin called app (relative path: ../app/). In the app directory there is a directory called plugins, in which there is a directory for the plugin. This is the compiled plugin. You can copy this directory in your GroIMP plugin directory (or in /home/.grogra.de-platform/plugins) and start GroIMP.

## Quick start

The plugin includes two example projects to get started. They can be launched from GroIMP from the embedded examples. They also are described in the wiki [point cloud as organ](https://wiki.grogra.de/doku.php?id=tutorials:using-mesh-clouds-as-organ) and [validation from point cloud](https://wiki.grogra.de/doku.php?id=tutorials:using-point-cloud-to-validate-model).

## Additional documentation

More documentation on both the implementation and design is available in the [wiki](https://wiki.grogra.de/doku.php?id=groimp-platform:pointcloud).


## PointCloud object in GroIMP

The point clouds in GroIMP uses the following interfaces:

  * **PointCloud** : A **Node**, i.e. an object that can be inserted in a GroIMP graph. The PointCloud is responsible for the display of the points by implementing **CollectionDisplayable**. 
  * **Cloud** : The representation of the data structure through which the point cloud data is accessed. 
  * **Point** : The base interface that an object must implements to be part of a Cloud. In GroIMP, points are not limited to 3 dimensions.
  

These three Objects implements **CloudContext**, meaning when any of the three is selected, you can access the **Cloud** structure handled.
Most of the tools from the package __de.grogra.pointcloud.tools__ are applied on Cloud, and produce Cloud objects. 
The GUI commands however can works with any CloudContext object.


## CloudArray and CloudGraph

GroIMP support two ways of defining a point cloud object:
 
  - As an array: the points coordinates are stored in the array, and the object is considered as one node in the project graph.
  - As a graph: each point is built in a node of the project graph. This representation enables links between nodes and possible meshes imported (e.g. ply files can support point, line and faces).

Whatever Cloud object you use, it will be embeeded in a PointCloud Node to be added to the Graph.

Despite not being Node, Cloud object can be directly added in a production rule (or an instantiation rule). In that case it will be automatically wrapped in a PointCloud Node and processed by the GraphManager.

```java 
Cloud c = new CloudList(new float[]{1,2,3});
[ Axiom ==> c; ]
```

result in the same as: 
```java 
Cloud c = new CloudList(new float[]{1,2,3});
PointCloud pc = new PointCloudD(c);
[ Axiom ==> pc; ]
```

## I/O

### Import 

Point clouds can be added to the project graph from GUI import as either: CloudList  or CloudGraph.
The supported file formats are: 

  * PLY: with vertices, and possibly edges, and faces. If imported as CloudGraph, the edges and faces will be linked to the vertices Points with refinement edges.
  * XYZ: simple x, y, z format. Each line starts with three values x y and z, separated by spaces, coma, or semi column. All additional values in the row will not be considered.

Point clouds can be loaded from RGG code with: 
```java 
// This do the same as the GUI menu>import from file
importNodeFromFile( "/path/to/file/leaf1.xyz", "mimeType/astext");
// This load the Node into a variable but do not adds it to the graph
 Node x = loadNodeFromFile( "/path/to/file/leaf1.xyz", "mimeType/astext");
```

The possible mimetype to use are (replace the "mimeType/astext" by):

  * model/x-grogra-pointcloud-graph+ply : for PLY file and CloudGraph
  * model/x-grogra-pointcloud-graph+xyz : for XYZ file and CloudGraph
  * model/x-grogra-pointcloud-array+ply : for PLY file and CloudList
  * model/x-grogra-pointcloud-array+xyz : for XYZ file and CloudList
  
### Export 

Point clouds can be exported separately from the scene using GUI commands in 3d view > View > Export ... > Point clouds > .
There are three possible export commands: 

  * Export all in one: All the point clouds in the scene are exported into one file. The user needs to select one file in the GUI prompt. This method export the **Global coordinates of the Points**.
  * One per file: Each point cloud is exported to a separate file. For each point cloud a new prompt from the GUI will is required. This method export the **Local coordinates of the Points**.
  * Selected: Export the selected point clouds (can be several). Each point cloud prompt a GUI file selection. This method export the **Local coordinates of the Points**.
  
## Tools

Point clouds comes with a set of basic tools which are:

  * Split : by Points or by Plane. This split a point clouds into two based on the condition.
  * Merge : merge several Point clouds into one. Converting the Points if needed.
  * Convert : convert a Cloud object into another (List to Array or Graph).
  * Cluster : Split a Point cloud based on a clustering algorithm. Two algorithms are implemented: k means, and dbscan.

The tools can be used from the GUI, in menu > Edit > point clouds > .
Or using RGG code:

```java 
// Selecting some Point clouds from the graph
Cloud c = first((*PointCloud*)).getCloud();
// split by plane
Cloud[] clouds = Tools.split(new Plane(), c);
// split by Points (in this example the Cloud is of the type CloudGraph)
Cloud[] clouds = Tools.split(slice((*c.getNode() (-->)*LeafPointImpl*),0,3), c);
// Merge all the Point Clouds
Tools.merge(((*PontCloud*)));
```
