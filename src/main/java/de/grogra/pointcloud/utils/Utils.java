package de.grogra.pointcloud.utils;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Extent;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudArray;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Plane;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.imp3d.objects.Transformation;
import de.grogra.math.TMatrix4d;
import de.grogra.math.TVector3d;
import de.grogra.mesh.renderer.handler.CollectionDisplayable;
import de.grogra.mesh.renderer.handler.CollectionMeshHandler;
import de.grogra.persistence.Transaction;
import de.grogra.pf.ui.Context;
import de.grogra.pointcloud.objects.CloudGraphBase;
import de.grogra.pointcloud.objects.impl.CloudGraph;
import de.grogra.pointcloud.objects.impl.CloudList;
import de.grogra.pointcloud.objects.impl.PointCloudD;
import de.grogra.pointcloud.tools.Tools;
import de.grogra.pointcloud.utils.Visitors.TransformVisitor;
import de.grogra.vecmath.Matrix34d;

public class Utils {
	
	/**
	 * Push the local transformation of the PointCloud to its Points. This enables to
	 * apply transformation tools to the pointcloud and then include the transformation
	 * on the points for other operations (using tools)
	 */
	public static void pushTransformToPoints(PointCloud pc) {
		if (!(pc instanceof Null)) { return; }
		Matrix4d transform = ((Null)pc).getLocalTransformation();
		transform.m33 = 1;
		Matrix4d ident = new Matrix4d();
		ident.setIdentity();
		if (transform.equals(ident)) { return;	}
		
		if (pc.getCloud() instanceof CloudGraph) {
			TransformVisitor v = new TransformVisitor(transform);
			v.init(((Null)pc).getCurrentGraphState(),  new EdgePatternImpl
					(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
			((Null)pc).getGraph().accept(pc, v, null);
		} else if (pc.getCloud() instanceof CloudList) {
			for (Cloud.Point point : pc.getCloud().getPoints()) {
				float[] all = point.toFloat();
				int i=0; // for every iteration of 3 values transform 
				int j=0; // if the Point has more that 3 values
				float[] pos = new float[3];
				for (float f : all) {
					if (i==2) {
						pos[i] = f;
						Point3f p = new Point3f(pos[0], pos[1], pos[2]);
						transform.transform(p);
						all[j*3 + 0] = p.x;
						all[j*3 + 1] = p.y;
						all[j*3 + 2] = p.z;
						j++;
						i=0;
					} else {
						pos[i] = f;
						i++;
					}
				}
			}
		} else if (pc.getCloud() instanceof CloudArray) {
			return;
		}
		TVector3d a = new TVector3d(0,0,0); 
		((Null)pc).setTransform(a);
//		((Null)pc).setTransform((TMatrix4d)null);
	}
	
	/**
	 * If the cloud is non editable, create a new ediable one with the points, 
	 * and dispose of the old one.
	 */
	public static Cloud ensureEditable(Cloud cloud) {
		Cloud oldCloud = null;
		if (!cloud.editable()) {
			oldCloud = Tools.convert(cloud, CloudList.class);
			cloud.dispose(true);
			cloud = oldCloud;
		}
		return cloud;
	}
	
//	/**
//	 * Translate the pointcloudbase to use the coordinate as new origin based on the newOrigin leaf provided.
//	 * @param pc
//	 * @param newOrigin
//	 * @param coordinate
//	 */
//	public static void setAsOrigin(CloudGraphBase pc, PointCloudLeaf newOrigin, Tuple3d coordinate ) {
//		if (!(newOrigin instanceof Null)) {
//			return;
//		}
//		Vector3d t = ((Null)newOrigin).getTranslation();
//		Vector3d translation = new Vector3d();
//		translation.x = coordinate.x - t.x;
//		translation.y = coordinate.y - t.y;
//		translation.z = coordinate.z - t.z;
//		
//		TranslateVisitor v = new TranslateVisitor(translation);
//		v.init(pc.getCurrentGraphState(),  new EdgePatternImpl
//				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
//		pc.getGraph().accept(pc, v, null);
//	}
//	
//	public static void setAsOrigin(Node[] pcs, PointCloudLeaf newOrigin, Tuple3d coordinate ) {
//		for (Node pc : pcs) {
//			if (pc instanceof CloudGraphBase) {
//				setAsOrigin((CloudGraphBase)pc, newOrigin, coordinate);
//			}
//		}
//	}
		
	/**
	 * Return the PointCloudBase that have the given source name (i.e. the pointcloud created from a file).
	 * One file usually create several pointcloudbase objects (one for the points, one for the faces, one for the edges).
	 * @param name
	 * @return
	 */
	public static Node[] fromSourceName(Graph g, String name) {
		name = Paths.get(name).getFileName().toString();
		
		ArrayList<Node> result = new ArrayList<Node>();
		Extent e = ((GraphManager)g).getExtent(de.grogra.imp3d.objects.PointCloud.class);
		for (Node n = e.getFirstNode (0); n != null; n = e.getNextNode (n))
		{
			if (n instanceof PointCloud && ((PointCloud)n).getCloud() instanceof CloudGraphBase) {
				CloudGraphBase c = (CloudGraphBase) ((PointCloud)n).getCloud() ;
				if (Paths.get(c.getSourceName()).getFileName().toString().equals(name)) {
					result.add(n);
				}
			}
		}
		return result.toArray(new Node[0]);
	}
	

	/**
	 * Adds a point cloud to the current context's graph
	 * 
	 * Obsolete. A Cloud can be added in the graph with XL command [ F ==> F c] with c a cloud.
	 * The node is automatically added.
	 * 
	 * @param pc. Pointcloud which will be added
	 * @param context. The current context
	 */
	public static void addCloudToGraph(Cloud pc, Node parent, Context context) {
		
		if (pc.getNode()!=null && pc.getNode().getGraph()!=null
				&& pc.getNode().getAxisParent() != null) {
			return;
		}
		if (parent == null) {
			parent = (Node) context.getWorkbench().getRegistry()
					.getProjectGraph().getRoot(GraphManager.MAIN_GRAPH);
		}
		Node cloudNode = pc.getNode();
		Transaction t = context.getWorkbench().getRegistry()
		.getProjectGraph().getActiveTransaction();
		if (cloudNode==null) {
			if ( pc instanceof CloudGraphBase && cloudNode instanceof CollectionDisplayable) {
				((CollectionDisplayable)cloudNode).setDisplayHandler(new CollectionMeshHandler());
//				cloudNode.addEdgeBitsTo((Node)pc, Graph.BRANCH_EDGE, t);
			} else if (pc instanceof CloudArray) {
				
				cloudNode = new PointCloudD(pc);
//				cloudNode.addEdgeBitsTo(cloud, Graph.BRANCH_EDGE, t);
			}
			else {
				cloudNode = new PointCloudD(pc.pointsToFloat());
//				cloudNode.addEdgeBitsTo(cloud, Graph.BRANCH_EDGE, t);
			}
		}
		parent.addEdgeBitsTo(cloudNode, Graph.BRANCH_EDGE, t);

	}
	
	/**
	 * Returns true if the point is on one side of the given plane and false if
	 * it is on the other side
	 *
	 * @param point The point to check the position of
	 * @param plane The plane to check whether the point is on the one side or
	 * on the other side
	 * @return true If the point is on one side of the plane and false otherwise
	 */
	public static boolean isPointOverPlane(Cloud.Point point, Plane plane) {
		// matrix.m01, matrix.m02 and matrix.m03 are the normal vector
		//Matrix4d matrix = plane.getLocalTransformation();
		Matrix4d matrix =GlobalTransformation.get (plane, true, GraphState.current (plane
				.getGraph ()), true).toMatrix4d();
		List<Point3d> points = new ArrayList<Point3d>();
		int j = 0;
		float[] pos = point.toFloat();
		double[] d = new double[3];
		for (int i = 0; i<pos.length; i++){
			d[j]=(double)pos[i];
			if (j==2) {
				points.add(new Point3d(d));
				j=-1;
			}
			j++;
		}
		
		boolean allOver = false; 
		j=0;
		for (Point3d myPoint : points) {
			if (point.getCloud() != null && point.getCloud().getNode() != null) {
				Matrix34d m = getGlobalTransform((PointCloud)point.getCloud().getNode());
				m.toMatrix4d().transform(myPoint);
			}
			//	Vector3d position = plane.getTranslation();
			Vector3d position  = new Vector3d (matrix.m03,matrix.m13, matrix.m23);
			Vector3d normal = new Vector3d(matrix.m02, matrix.m12, matrix.m22);
			position.sub(myPoint);
			boolean res = position.dot(normal) > 0;
			if (j > 0 && res != allOver) {
				return false;
			}
			allOver = res;
			j++;
		}
		return allOver;
	}
	
	
	public static Matrix34d getGlobalTransform(PointCloud pc) {
		Matrix4d out = new Matrix4d();
		out.setIdentity();
		Matrix4d in = new Matrix4d();
		in.setIdentity();
		
		
		List<Null> l = new ArrayList<Null>();
		l.add((Null)pc);
		Node n = (Node)pc;
		while ( ( n = getParent(n)) != null) {
			if (n instanceof Null) {
				l.add((Null)n);
			}
		}
		GraphState gs = ((Node)pc).getCurrentGraphState();
		for (int i=l.size()-1; i>=0; i--) {
			n = l.get(i);
			Transformation t = (Transformation) gs.getObjectDefault
				(n, true, Attributes.TRANSFORMATION, null);
			if (t != null)
			{
				t.preTransform (n, true, out, in, gs);
				t.postTransform (n, true, in, out, out, gs);
			}
		}
		Matrix34d res = new Matrix34d();
		res.set(out);
		return res;
	}
	
	private static Node getParent(Node n) {
		Node parent = null;
		for (Edge e = n.getFirstEdge(); parent == null && e != null; e = e.getNext(n)) {
			if (e.isTarget(n) && !e.isSource(n)) {
				parent = e.getSource();
			}
		}
		return parent;
	}
}
