package de.grogra.pointcloud.utils;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4d;

import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.math.TMatrix4d;
import de.grogra.math.Transform3D;
import de.grogra.mesh.utils.FloatListArray;
import de.grogra.persistence.Transaction;
import de.grogra.pointcloud.objects.IntermediateCloudNode;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.pointcloud.objects.impl.LeafPointImpl;

public class Visitors {

	static class TransformVisitor extends VisitorImpl {

		Matrix4d transform;

		TransformVisitor (Matrix4d m){
			super();
			this.transform = m;
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof Cloud.Point) {
					if (n instanceof Null) { // if n is a Null modify it through transformation
						Matrix4d res = new Matrix4d();
						Transform3D t = ((Null)n).getTransform();
						if (t == null) { t=new TMatrix4d(); }
						t.transform(transform, res);
						if (n instanceof LeafPointImpl) { // points get a vector to save memory
							((Null)n).setTransform(new javax.vecmath.Point3d(res.m03, res.m13, res.m23));
						} else {
							((Null)n).setTransform(res);
						}
					} 
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}

	public static class GetPointVisitor extends VisitorImpl {

		public List<PointCloudLeaf> leaves = new ArrayList<PointCloudLeaf>();

		public GetPointVisitor (){
			super();
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof PointCloudLeaf) {
					leaves.add((PointCloudLeaf) n);
					return STOP;
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}

	public static class RemovePointVisitor extends VisitorImpl {

		Transaction t;
		
		public RemovePointVisitor (Transaction t){
			super();
			this.t=t;
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof PointCloudLeaf) {
					((Node)n).removeAll(t);
					return STOP;
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}


	public static class GetPointCloudVisitor extends VisitorImpl {

		public List<PointCloud> bases = new ArrayList<PointCloud>();

		public GetPointCloudVisitor (){
			super();
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof PointCloud) {
					bases.add((PointCloud) n);
					return STOP;
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}

	public static class GetFloatFromPointVisitor extends VisitorImpl {

		public FloatListArray vertices = new FloatListArray();

		public GetFloatFromPointVisitor (){
			super();
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof PointCloudLeaf) {
					for (float f : ((Cloud.Point)n).toFloat()) {
						vertices.push(f);
					}
					return STOP;
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}

	public static class GetBoundariesVisitor extends VisitorImpl {

		public double minx, miny, minz, maxx, maxy, maxz;		
		public GetBoundariesVisitor (){
			super();
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof Cloud.Point) {
					float[] pos = ((Cloud.Point)n).toFloat();
					int i = 0;
					if (pos.length<3) { return STOP; }
					for (float f : pos) {
						if (i == 0) {
							minx = min(minx, f);
							maxx = max(maxx, f);
							i=1;
						} else if (i==1) {
							miny = min(miny, f);
							maxy = max(maxy, f);
							i=2;
						} else if (i==2) {
							minz = min(minz, f);
							maxz = max(maxz, f);
							i=0;
						}
					}
					return STOP;
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}
	
	public static class GetFirstLeafVisitor extends VisitorImpl {

		public PointCloudLeaf first=null;
		boolean found = false;

		public GetFirstLeafVisitor (){
			super();
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (found) { return STOP; }
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof PointCloudLeaf) {
					first = (PointCloudLeaf) n;
					found=true;
					return STOP;
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}
	
	public static class GetFirstIPCNVisitor extends VisitorImpl {

		public IntermediateCloudNode first=null;
		public long max;

		public GetFirstIPCNVisitor (long m){
			super();
			max = m;
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof IntermediateCloudNode) {
					if (n.getDirectChildCount() < max) {
						first = (IntermediateCloudNode) n;
						return STOP;
					}
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}
}
