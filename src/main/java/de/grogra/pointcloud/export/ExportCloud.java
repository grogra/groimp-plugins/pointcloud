package de.grogra.pointcloud.export;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4d;

import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.imp3d.objects.Transformation;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.vecmath.Matrix34d;

/**
 * Base for exporting a single pointcloud from a Node. 
 * The source should be an ObjectSource with a CloudContext as object.
 */
public abstract class ExportCloud extends FilterBase  {

	/**
	 * Global transformation of the PointCloud node.
	 */
	protected Matrix4d transformOfCloud;
	
	public ExportCloud(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(item.getOutputFlavor()); 
	}

	public void export(PointCloud pc) {
		if (transformOfCloud==null) {
			if ( pc instanceof Null ) { 
//				Matrix34d m = getTransformFromPath(pc);
//				transformOfCloud = new Matrix4d();
//				m.get(transformOfCloud);
				transformOfCloud = new Matrix4d();
				transformOfCloud.setIdentity();
				Matrix4d m = ((Null)pc).getLocalTransformation();
				transformOfCloud.mul(m);
			} else {
				transformOfCloud = new Matrix4d();
				transformOfCloud.setIdentity();
			}
		}
		exportImpl(pc);
	}
	public abstract void exportImpl(PointCloud pc);
	
	
}
