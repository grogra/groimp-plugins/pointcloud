package de.grogra.pointcloud.export;

import java.io.IOException;

import javax.vecmath.Matrix4d;

import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.io.SceneGraphExport.NodeExport;
import de.grogra.imp3d.objects.SceneTree.InnerNode;

public abstract class Leaf implements NodeExport {

	Matrix4d transformation;
	
	@Override
	public void export(de.grogra.imp3d.objects.SceneTree.Leaf node, InnerNode transform, SceneGraphExport sge)
			throws IOException {
		ExportPointCloud export = (ExportPointCloud) sge;
		Matrix4d m = export.matrixStack.peek ();
		Matrix4d n = new Matrix4d ();
		if (transform != null) {
			transform.transform (m, n);
		} else {
			n.set (m);
		}
		transformation = n;
		exportImpl(node, transform, export);
	}
	
	public abstract void exportImpl (de.grogra.imp3d.objects.SceneTree.Leaf node, InnerNode transform, ExportPointCloud export) throws IOException;

	protected Matrix4d getTransformation () {
		return transformation;
	}

}
