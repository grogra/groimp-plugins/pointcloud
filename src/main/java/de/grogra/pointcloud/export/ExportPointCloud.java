package de.grogra.pointcloud.export;

import java.io.IOException;
import java.util.Stack;

import javax.vecmath.Matrix4d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pointcloud.objects.CollectionCloud;

/**
 * Base for exporting pointclouds from the scene.
 */
public class ExportPointCloud extends SceneGraphExport {

	CollectionCloud currentCollection;
	PointCloud currentCloud;
	
	final Stack<Matrix4d> matrixStack = new Stack<Matrix4d>();

	
	public ExportPointCloud(FilterItem item, FilterSource source) {
		super(item, source);
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		matrixStack.push(m);
		
	}

	@Override
	protected SceneTree createSceneTree(View3D scene) {
		SceneTree t = new SceneTree (scene) {

			@Override
			protected boolean acceptLeaf(Object object, boolean asNode) {
				return getExportFor(object, asNode) != null;
			}

			@Override
			protected Leaf createLeaf(Object object, boolean asNode, long id) {
				Leaf l = new Leaf (object, asNode, id);
				init(l);
				return l;
			}
		};
		t.createTree(true,false);
		return t;
	}

	@Override
	public NodeExport getExportFor (Object object, boolean asNode)
	{
		if (!asNode) { return null; }
		if (object instanceof Node && ((Node)object).getIgnored()) {
			return null;
		}
		NodeExport ex = super.getExportFor (object, asNode);
		if (ex != null) return ex;
		return null;
	}

	@Override
	protected void beginGroup (InnerNode group) throws IOException {
		Matrix4d m = matrixStack.peek();
		Matrix4d n = new Matrix4d();
		group.transform(m, n);
		matrixStack.push(n);
	}
	@Override
	protected void endGroup (InnerNode group) throws IOException {
		matrixStack.pop();
	}



}
