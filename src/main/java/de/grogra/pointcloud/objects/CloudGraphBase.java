package de.grogra.pointcloud.objects;

import de.grogra.imp3d.objects.BoundedCloud;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.persistence.Manageable;

/**
 * CloudGraph is a cloud data structure that represent it "points" as 
 * node in the project graph. The points are positioned at the leaves of this 
 * subgraph. The intermediate nodes are not holding data from the Cloud.
 * Leaves can be any object that implements Cloud.Point and extends Node.
 * 
 * CloudGraph cannot manipulate points if it is not linked to a Node
 */
public abstract class CloudGraphBase implements BoundedCloud, Manageable {
	
	@Override public Cloud getCloud() { return this; }
	
	/*
	 * Add intermediate nodes to the pointcloud. The intermediate nodes should
	 * have leaves children.
	 */
	public abstract void addPoint(IntermediateCloudNode[] node);
	public abstract void addPoint(IntermediateCloudNode node);
	
	/*
	 * Rebalance the graph based on the numbers of node
	 */
	public abstract void rebalance();
	
	/*
	 * Name of the source file / attribute from source? 
	 */
	public abstract String getSourceName();
	
	/*
	 * Implementation type of the nodes (points). By default use {PointCloudNode} - to be defined
	 */
	protected Class<? extends PointCloudLeaf> childrenType;
	public abstract Class getChildrenType() throws ClassNotFoundException;
		
	
	public abstract PointCloudLeaf getFirstLeaf();
	

}
