package de.grogra.pointcloud.objects;

import de.grogra.imp3d.objects.PointCloud;

/**
 * This object represent a collection of several clouds
 */
public interface CollectionCloud {
	public PointCloud[] getClouds();
}
