package de.grogra.pointcloud.objects;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.PointCloud;

/**
 * All points in pointclouds should implements this AND extends Node
 * Probably should be a class. The shape should be a attribute (e.g. representativeType = Sphere).
 */
public interface PointCloudLeaf extends Cloud.Point {
	
	public abstract Node getNode();
	
	/*
	 * The id of the object from the import file - The same one is used in the point cloud
	 */
	public long getIdImport();
	public void setIdImport (long value);
	

	@Override
	public default Cloud getCloud() {
		Node p = getNode().getAxisParent();
		while (p != null) {
			if (p instanceof PointCloud) {
				return (Cloud) ((PointCloud)p).getCloud();
			}
			p = p.getAxisParent();
		}
		return null;
	}
	
	//TODO ?
	@Override
	public default void setCloud(Cloud c) {
	}
}
