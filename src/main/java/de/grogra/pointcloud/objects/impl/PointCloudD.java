package de.grogra.pointcloud.objects.impl;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.imp.PickList;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudArray;
import de.grogra.imp3d.objects.PointCloudImpl;
import de.grogra.mesh.renderer.handler.CollectionDisplayHandler;
import de.grogra.mesh.renderer.handler.CollectionDisplayable;
import de.grogra.mesh.renderer.handler.FloatArrayHandler;
import de.grogra.mesh.renderer.handler.Updatable;

/**
 * Point Cloud Node that wraps the display behind a CollectionDisplay.
 */
public class PointCloudD extends PointCloudImpl implements Updatable, CollectionDisplayable {
	
	protected CollectionDisplayHandler viewhandler;
	//enh:field setmethod=setDisplayHandler getmethod=getDisplayHandler
	
	transient protected boolean update;
	transient protected boolean initialized = false;

	
	public PointCloudD() {
		cloud = new CloudArray();
	}

	public PointCloudD(Cloud c) {
		cloud = c;
		c.setNode(this);
	}

	public PointCloudD(float[] f) {
		cloud = new CloudArray(this, f);
	}
		
	/**
	 * The display and picking are managed by the DisplayHandler
	 */
	@Override
	public CollectionDisplayHandler getDisplayHandler() {
		if (viewhandler == null){
			viewhandler=new FloatArrayHandler();
			viewhandler.setNode(this);
			setResolution(0);
		}
		return viewhandler;
	}

	@Override
	public void setDisplayHandler(CollectionDisplayHandler d) {
		viewhandler = d;
		viewhandler.setNode(this);
		setResolution(3);
	}

	
	@Override
	public void setUpdate(boolean b) {
		if (getDisplayHandler() != null) {
			viewhandler.setUpdate(b);
			update = b;
		}
	}
	@Override 
	public void update() {
		de.grogra.pointcloud.utils.Utils.pushTransformToPoints(this);
		setUpdate(true);
	}
	@Override 
	public boolean getUpdate() {
		if (getDisplayHandler() != null) {
			return viewhandler.getUpdate();
		} 
		return update;
	}
	@Override
	public State getState() {
		if (getDisplayHandler() != null) {
			return viewhandler.getState();
		}
		return null;
	}
	@Override
	public void setState(State s) {
		if (getDisplayHandler() != null) {
			viewhandler.setState(s);
		}
	}
	
	protected void initialize() {
		if (initialized) {return;}
		this.initialized = true;
		de.grogra.pointcloud.utils.Utils.pushTransformToPoints(this);
	}

	@Override
	public void draw(Object object, boolean asNode, RenderState rs) {
		if (getDisplayHandler() != null) {
			viewhandler.draw(object, asNode, rs);
		}
	}

	@Override
	public void pick(Object object, boolean asNode, Point3d origin, Vector3d direction, Matrix4d transformation,
			PickList list) {
		if (getDisplayHandler() != null) {
			viewhandler.pick(object, asNode, origin, direction, transformation, list);
		}
	}
	
	
//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field viewhandler$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (PointCloudD.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((PointCloudD) o).setDisplayHandler ((CollectionDisplayHandler) value);
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((PointCloudD) o).getDisplayHandler ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new PointCloudD ());
		$TYPE.addManagedField (viewhandler$FIELD = new _Field ("viewhandler", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CollectionDisplayHandler.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new PointCloudD ();
	}

//enh:end
}
