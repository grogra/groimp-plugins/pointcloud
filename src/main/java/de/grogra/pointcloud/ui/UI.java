package de.grogra.pointcloud.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.io.FilenameUtils;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Extent;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp.View;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.BoundedCloud;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudArray;
import de.grogra.imp3d.objects.CloudContext;
import de.grogra.imp3d.objects.Plane;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.mesh.renderer.handler.CollectionDisplayable;
import de.grogra.mesh.renderer.handler.Updatable;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.edit.GraphSelection;
import de.grogra.pf.ui.edit.GraphSelectionImpl;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.pointcloud.objects.CollectionCloud;
import de.grogra.pointcloud.objects.impl.CloudGraph;
import de.grogra.pointcloud.objects.impl.CloudList;
import de.grogra.pointcloud.tools.DBSCAN;
import de.grogra.pointcloud.tools.KMeans;
import de.grogra.pointcloud.tools.Tools;
import de.grogra.pointcloud.utils.Utils;
import de.grogra.pointcloud.utils.Visitors.GetPointCloudVisitor;
import de.grogra.util.MimeType;
import de.grogra.xl.lang.Aggregate;
import de.grogra.xl.util.ObjectList;

/**
 * This class contains methods useful for ui interactions.
 * All Methods are static.
 * 
 */
public class UI {

	/**
	 * Flavor used to export all point cloud as one.
	 */
	public static final IOFlavor ALLCLOUD_FLAVOR = IOFlavor.valueOf (PointCloud.class);
	/**
	 * Flavor used to export point clouds as single file.
	 */
	public static final IOFlavor ONECLOUD_FLAVOR = IOFlavor.valueOf (Cloud.class);

	/**
	 * Commands to export all PointClouds into separate files.
	 */
	public static void exportSplitFile(Item item, Object info, Context ctx) throws Exception {
		if (!(info instanceof ActionEditEvent)){
			return;
		}
		ctx.getWorkbench().setProperty(ctx.getWorkbench(). EXPORT_VISIBLE_LAYER, false);
		ActionEditEvent ae = (ActionEditEvent)info;

		GetPointCloudVisitor v = new GetPointCloudVisitor();
		v.init(GraphState.current (ctx.getWorkbench().getRegistry().getProjectGraph()) , new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		ctx.getWorkbench().getRegistry().getProjectGraph().accept(
				ctx.getWorkbench().getRegistry().getProjectGraph().getRoot(GraphManager.MAIN_GRAPH), v, null);
		List<PointCloud> pc = v.bases;
		List<Cloud> alreadyExportedClouds = new ArrayList<Cloud>();
		for (PointCloud n : pc) {
			if (!alreadyExportedClouds.contains(((CloudContext)n).getCloud())) {
				Node parent = ((Node)n).getAxisParent();
				if (parent instanceof CollectionCloud) {
					PointCloud[] pcs = ((CollectionCloud)parent).getClouds();
					for (PointCloud a : pcs) {
						alreadyExportedClouds.add(a.getCloud());
					}
					export (toFilterSource ((CollectionCloud)parent, ctx), ctx.getWorkbench(), 
							"Export collection : "+((parent.getName()!=null)? parent.getName() : parent.getId()));
				} else {
					alreadyExportedClouds.add(((CloudContext)n).getCloud());
					export (toFilterSource ((CloudContext)n, ctx), ctx.getWorkbench(), "Export cloud : "+((PointCloud)n).getCloud().getNode().getName());
				}
			}
		}
	}
	/**
	 * Export the selected PointCloud
	 */
	public static void exportSelected(Item item, Object info, Context ctx) throws Exception {
		if (!(info instanceof ActionEditEvent)){
			return;
		}
		ctx.getWorkbench().setProperty(ctx.getWorkbench(). EXPORT_VISIBLE_LAYER, false);
		ActionEditEvent e = (ActionEditEvent)info;
		List<Cloud> alreadyExportedClouds = new ArrayList<Cloud>();
		List<Node> nodes = getSelectedNodes(ctx);
		for (Node n : nodes) {
			if (n instanceof CollectionCloud) {
				PointCloud[] pcs = ((CollectionCloud)n).getClouds();
				for (PointCloud a : pcs) {
					alreadyExportedClouds.add(a.getCloud());
				}
				export (toFilterSource ((CollectionCloud)n, ctx), ctx.getWorkbench(), 
						"Export collection : "+((n.getName()!=null)? n.getName() : n.getId()));
			}
			else if (n instanceof CloudContext && !alreadyExportedClouds.contains(((CloudContext)n).getCloud())) {
				alreadyExportedClouds.add(((CloudContext)n).getCloud());
				export (toFilterSource ((CloudContext)n, ctx), ctx.getWorkbench(), 
						"Export cloud : "+((((PointCloud)n).getCloud().getNode().getName()!=null)? 
								((PointCloud)n).getCloud().getNode().getName() : 
									((PointCloud)n).getCloud().getNode().getId()));
			}
		}
	}
	/**
	 * Command to export All pointclouds in the scene into one file.
	 */
	public static void exportOneFile(Item item, Object info, Context ctx) throws Exception {
		if (!(info instanceof ActionEditEvent)){
			return;
		}
		ctx.getWorkbench().setProperty(ctx.getWorkbench(). EXPORT_VISIBLE_LAYER, false);
		ActionEditEvent e = (ActionEditEvent)info;
		ctx.getWorkbench().export (toFilterSource ((View) e.getPanel ()));
	}
	private static FilterSource toFilterSource (View view)
	{
		return new ObjectSourceImpl (view, "view", ALLCLOUD_FLAVOR, view
				.getWorkbench ().getRegistry ().getRootRegistry (), null);
	}
	private static FilterSource toFilterSource (CloudContext[] clouds, Context ctx)
	{
		return new ObjectSourceImpl (clouds, "clouds", ONECLOUD_FLAVOR, ctx
				.getWorkbench ().getRegistry ().getRootRegistry (), null);
	}
	private static FilterSource toFilterSource (CloudContext cloud, Context ctx)
	{
		return new ObjectSourceImpl (cloud, "cloud", ONECLOUD_FLAVOR, ctx
				.getWorkbench ().getRegistry ().getRootRegistry (), null);
	}
	private static FilterSource toFilterSource (CollectionCloud cloud, Context ctx)
	{
		return new ObjectSourceImpl (cloud, "cloud", ONECLOUD_FLAVOR, ctx
				.getWorkbench ().getRegistry ().getRootRegistry (), null);
	}

	/**
	 * TODO: add this export to the wokrbennch class
	 */
	private static void export(FilterSource src, Workbench wb, String title) {
		FileChooserResult fr = wb.chooseFileToSave(title, src.getFlavor(), null);
		if (fr != null) {
			wb.export(src, fr.getMimeType(), fr.file);
		}
	}

	/**
	 * Export the given cloud to the file. It looks at the file extension to get 
	 * the export format.
	 */
	public static void export(CloudContext cloud, File file) {
		Workbench wb = Workbench.current();
		FilterSource src = toFilterSource(cloud.getCloud(), wb);
		String ext = FilenameUtils.getExtension(file.getName());
		if (ext.contentEquals("ply")) {
			wb.export(src, new MimeType("model/x-grogra-cloud+ply"), file);
		} else {
			// should be "xyz" but we use xyz by default.
			wb.export(src, new MimeType("model/x-grogra-cloud+xyz"), file);
		}
	}
	
	public static void export(CloudContext[] clouds, File file) {
		Workbench wb = Workbench.current();
		FilterSource src = toFilterSource(clouds, wb);
		String ext = FilenameUtils.getExtension(file.getName());
		if (ext.contentEquals("ply")) {
			wb.export(src, new MimeType("model/x-grogra-cloud+ply"), file);
		} else {
			// should be "xyz" but we use xyz by default.
			wb.export(src, new MimeType("model/x-grogra-cloud+xyz"), file);
		}
	}
	
	public static void export(Aggregate a, CloudContext value, File file) {
		if (a.initialize ())
		{
			a.aval1 = new ObjectList ();
			a.aval2 = file;
		}
		if (a.isFinished ())
		{
			Cloud[] array = new Cloud[((ObjectList) a.aval1).size];
			int i = 0;
			for (Object o : ((ObjectList) a.aval1)) {
				array[i] = (Cloud)o;
				i++;
			}
			export (array, (File) a.aval2);
		}
		else
		{
			((ObjectList) a.aval1).addIfNotContained(value.getCloud());
		}
	}

	/**
	 * This method is called from the user by the user interface. 
	 * It splits the selected point cloud by a selected plane.
	 * 
	 * If a Cloud Node is selected instead of a Cloud, it takes the first Cloud of 
	 * that node. Imp3d Cloud only contains one Cloud for instance.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void splitByPlane(Item item, Object information, Context context) {
		de.grogra.pf.ui.UI.executeLockedly(context.getWorkbench().getRegistry().getProjectGraph(), true, 
				new Command() {

			@Override
			public String getCommandName() {

				return "splitByPlane";
			}

			@Override
			public void run(Object info, Context context) {
				GraphSelectionImpl selection = (GraphSelectionImpl)UIProperty.WORKBENCH_SELECTION.getValue(context);
				Cloud[] results = new Cloud[0];
				Node parent = null;
				if(selection.size()==2) {
					Cloud c=null;
					Plane p=null;

					for(int i=0; i<2; i++) {
						Object s = selection.getObject(i);
						if(s instanceof CloudContext) {
							c = ((CloudContext)s).getCloud();
							Utils.pushTransformToPoints((PointCloud)c.getNode());
							parent = c.getNode().getAxisParent();
						}
						if(s instanceof Plane) {
							p= (Plane)s;
						}
					}
					if(p!=null && c !=null) {
						results = Tools.split(p, c);
					}
				}
				for (Cloud cloud : results) {
					Utils.addCloudToGraph(cloud, parent, context);
				}

			}
		}			
		, information, context, JobManager.ACTION_FLAGS);
	}

	/**
	 * This method is called from the user by the user interface. 
	 * It splits a selected point cloud by all selected
	 * point cloud leaves.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void splitByLeaves(Item item, Object information, Context context) {
		de.grogra.pf.ui.UI.executeLockedly(context.getWorkbench().getRegistry().getProjectGraph(), true, 
				new Command() {

			@Override
			public String getCommandName() {

				return "splitByLeaf";
			}

			@Override
			public void run(Object info, Context context) {

				List<Node> objects = getSelectedNodes(context);
				Node parent = null;
				if(objects.size() < 2)
					return;

				Cloud pointcloud = null;
				List<Cloud.Point> leaves = new ArrayList<Cloud.Point>();

				for(int i = 0; i < objects.size(); i++) {
					if(objects.get(i) instanceof Cloud) {
						pointcloud = (Cloud) objects.get(i);
						Utils.pushTransformToPoints((PointCloud)pointcloud.getNode());
						parent = pointcloud.getNode().getAxisParent();
					}
					else if(objects.get(i) instanceof Cloud.Point)
						leaves.add((Cloud.Point) objects.get(i));
				}

				if(leaves.size() > 0) {
					Cloud[] pc = Tools.split(leaves.toArray(new Cloud.Point[0]), pointcloud);
					Utils.addCloudToGraph(pc[1], parent, context);
				}
			}
		}, information, context, JobManager.ACTION_FLAGS);
	}

	/**
	 * This method is called from the user by the user interface. It will merge all selected point clouds
	 * into the first selected point cloud.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void merge(Item item, Object information, Context context) {
		de.grogra.pf.ui.UI.executeLockedly(context.getWorkbench().getRegistry().getProjectGraph(), true, 
				new Command() {

			@Override
			public String getCommandName() {

				return "merge";
			}

			@Override
			public void run(Object info, Context context) {


				List<Node> objects = getSelectedNodes(context);

				if(objects.size() < 2)
					return;

				List<Cloud> pointclouds = new ArrayList<Cloud>();
				for(int i = 0; i < objects.size(); i++) {
					if(objects.get(i) instanceof CloudContext)
						pointclouds.add(((CloudContext) objects.get(i)).getCloud());
				}


				if(pointclouds.size() >= 2) {
					for (Cloud c : pointclouds) {
						Utils.pushTransformToPoints((PointCloud)c.getNode());
					}
					Object[] array = new Object[pointclouds.size()];
					int i = 0;
					for (Object o : pointclouds) {
						array[i] = (Cloud)o;
						i++;
					}
					Cloud pc = Tools.merge(array);
					
					
					if (pc.getNode() != null && pc.getNode() instanceof CollectionDisplayable) {
						((CollectionDisplayable)pc.getNode()).setUpdate(true);
					}
				}
			}
		}, information, context, JobManager.ACTION_FLAGS);
	}

	//	public static void convert(Item item, Object information, Context context) {
	//		List<Node> objects = getSelectedNodes(context);
	//		
	//		
	//		List<Cloud> pointclouds = new ArrayList<de.grogra.imp3d.objects.PointCloud>();
	//		for(int i = 0; i < objects.size(); i++) {
	//			if(objects.get(i) instanceof de.grogra.imp3d.objects.PointCloud)
	//				pointclouds.add((de.grogra.imp3d.objects.PointCloud) objects.get(i));
	//		}
	//		for(de.grogra.imp3d.objects.PointCloud pc : pointclouds) {
	//			float[]  points = pc.getPoints();
	//			PointCloudBaseImpl pcb = new PointCloudBaseImpl("",points.length/12,2,LeafPointImpl.class);
	//			for(int i=0; i<points.length/3; i+=3) {
	//				pcb.addPoint(new LeafPointImpl(points[i],points[i+1],points[i+2]));
	//			}
	//			addPointCloudToGraph(pcb,context);		
	//		}
	//		
	//	}


	/**
	 * This method is called from the user by the user interface. It will determine the selected point cloud
	 * and calls the K-Means clustering algorithm with user input data as parameters.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void kMeansCluster(Item item, Object information, Context context) {
		de.grogra.pf.ui.UI.executeLockedly(context.getWorkbench().getRegistry().getProjectGraph(), true, 
				new Command() {

			@Override
			public String getCommandName() {

				return "kMeansCluster";
			}

			@Override
			public void run(Object info, Context context) {

				List<Node> objects = getSelectedNodes(context);

				Cloud pointcloud = null;
				if(objects.size() != 1) {
					return;
				}

				if(objects.get(0) instanceof CloudContext) {
					pointcloud  = ((CloudContext) objects.get(0)).getCloud();
					Utils.pushTransformToPoints((PointCloud)pointcloud.getNode());
				} else {
					return;
				}
				Node parent = pointcloud.getNode().getAxisParent();
				List<String> labels = List.of("Cluster Count", "Maximum Iterations");
				List<Class<?>> types = List.of(Integer.class, Integer.class);

				JPanel panel = UIDialog.createDynamicInputPanel(labels, types);
				String title = "KMeans clustering";
				int buttons = JOptionPane.OK_CANCEL_OPTION;
				int type = JOptionPane.QUESTION_MESSAGE;
				int result = UIDialog.getInstance().displayParameterInput(panel, title, buttons, type);
				if(result == JOptionPane.OK_OPTION) {
					List<Object> inputs = UIDialog.getInputValues(panel, types);
					int clusterCount = (int) inputs.get(0);
					int maxIterationsCount = (int) inputs.get(1);

					Cloud[] cluster = KMeans.cluster(pointcloud, clusterCount, maxIterationsCount);

					//remove the current point cloud
					pointcloud.getNode().removeAll(pointcloud.getNode().getGraph().getActiveTransaction());
					//			pointcloud.getAxisParent().getAxisParent().removeAll(null);

					for(int i = 0; i < cluster.length; i++) {
						Utils.addCloudToGraph(cluster[i], parent, context);
					}
				}
			}
		}, information, context, JobManager.ACTION_FLAGS);
	}

	/**
	 * This method is called from the user by the user interface. It will determine the selected point cloud
	 * and calls the DBSCAN clustering algorithm with user input data as parameters.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void dbscanCluster(Item item, Object information, Context context) {
		de.grogra.pf.ui.UI.executeLockedly(context.getWorkbench().getRegistry().getProjectGraph(), true, 
				new Command() {

			@Override
			public String getCommandName() {

				return "dbscanCluster";
			}

			@Override
			public void run(Object info, Context context) {

				List<Node> objects = getSelectedNodes(context);

				BoundedCloud pointcloud = null;
				if(objects.size() != 1) {
					return;
				}

				if(objects.get(0) instanceof CloudContext) {
					Cloud c  = ((CloudContext) objects.get(0)).getCloud();
					if (c instanceof BoundedCloud) {
						pointcloud = (BoundedCloud) c;
						Utils.pushTransformToPoints((PointCloud)pointcloud.getNode());
					}
				} else {
					return;
				}
				Node parent = pointcloud.getNode().getAxisParent();
				List<String> labels = List.of("Point distance", "Minimum Neighbours", "Octree Depth", "Remove Noise");
				List<Class<?>> types = List.of(Double.class, Integer.class, Integer.class, Boolean.class);

				JPanel panel = UIDialog.createDynamicInputPanel(labels, types);
				String title = "DBSACN clustering";
				int buttons = JOptionPane.OK_CANCEL_OPTION;
				int type = JOptionPane.QUESTION_MESSAGE;
				int result = UIDialog.getInstance().displayParameterInput(panel, title, buttons, type);
				if(result == JOptionPane.OK_OPTION) {
					List<Object> inputs = UIDialog.getInputValues(panel, types);
					double eps = (double) inputs.get(0);
					int minimumNeighbours = (int) inputs.get(1);
					int octreeDepth = (int) inputs.get(2);
					boolean removeNoise = (boolean) inputs.get(3);

					Cloud[] clusters = DBSCAN.cluster(pointcloud, eps, minimumNeighbours, octreeDepth, removeNoise);

					//remove the current point cloud
					//			pointcloud.getAxisParent().getAxisParent().removeAll(null);
					pointcloud.getNode().remove(pointcloud.getNode().getGraph().getActiveTransaction());

					//adding the clusters to the scene
					for(Cloud c : clusters) {
						Utils.addCloudToGraph(c, parent, context);
					}

				}
			}
		}, information, context, JobManager.ACTION_FLAGS);
	}

	/**
	 * returns the selected nodes from the project graph
	 */
	public static List<Node> getSelectedNodes(Context context) {
		List<Node> res = new ArrayList<Node>();
		Workbench workbench = Workbench.current();
		if (workbench == null) {
			return res;
		}
		Object object = UIProperty.WORKBENCH_SELECTION.getValue(workbench);
		if (!(object instanceof GraphSelection)) {
			return res;
		}
		GraphSelection selection = (GraphSelection)(object);
		for (int i = 0; i< selection.size(); i++) {
			if (selection.isNode(i)) {
				res.add((Node)selection.getObject(i));
			}
		}
		return res;
	}

	/**
	 * Currently loop over 2 extents because the PointCloud class isn't extending Node.
	 * TODO: once PointCloud has been pushed back in IMP3d (or 1d? ) loop over that extent
	 */
	public static void selectAllPointClouds(Item item, Object information, Context context) {
		ArrayList<Node> result = new ArrayList<Node>();
		Extent e = context.getWorkbench().getRegistry().getProjectGraph().getExtent(de.grogra.imp3d.objects.PointCloud.class);
		for (Node n = e.getFirstNode (0); n != null; n = e.getNextNode (n))
		{
			if (n instanceof de.grogra.imp3d.objects.PointCloud ) {
				result.add(n);
			}
		}
		Node[] nodes = new Node[result.size()];
		for (int i = 0; i<result.size();i++) {
			nodes[i]=result.get(i);
		}

		context.getWorkbench().select(nodes);
	}


	public static void getCloud(Item item, Object info, Context ctx) {
		Workbench workbench = Workbench.current();
		if(workbench!=null) {
			Object object = UIProperty.WORKBENCH_SELECTION.getValue(workbench);
			if (object instanceof GraphSelection) {
				GraphSelection selection = (GraphSelection)(object);
				ObjectList<Node> nodes = new ObjectList<Node>();


				for (int i = 0; i< selection.size(); i++) {
					if (selection.isNode(i)) {
						if(selection.getObject(i) instanceof CloudContext) {
							nodes.add(((CloudContext)selection.getObject(i)).getCloud().getNode());
						}else {
							nodes.add((Node)selection.getObject(i));
						}
					}
				}
				Node[] list = new Node[nodes.size];
				nodes.toArray(list);
				workbench.select(list);
			}
		}
	}


	public static void updateDispaly(Item item, Object info, Context ctx) {
		List<Node> nodes = getSelectedNodes(ctx);
		for (Node n : nodes) {
			if (n instanceof CloudContext) {
				de.grogra.pointcloud.utils.Utils.pushTransformToPoints(
						(PointCloud)((CloudContext)n).getCloud().getNode());
				n = ((CloudContext)n).getCloud().getNode();
				if (n != null && n instanceof Updatable) {
					((Updatable)n).setUpdate(true);
				}
			}
		}
		View3D.getDefaultView(ctx).repaint();
	}
	
	public static void convertToArray(Item item, Object info, Context ctx) {
		de.grogra.pf.ui.UI.executeLockedly(ctx.getWorkbench().getRegistry().getProjectGraph(), true, 
				CONVERT , CloudArray.class, ctx, JobManager.ACTION_FLAGS);
	}
	
	public static void convertToList(Item item, Object info, Context ctx) {
		de.grogra.pf.ui.UI.executeLockedly(ctx.getWorkbench().getRegistry().getProjectGraph(), true, 
				CONVERT , CloudList.class, ctx, JobManager.ACTION_FLAGS);
	}

	public static void convertToGraph(Item item, Object info, Context ctx) {
		de.grogra.pf.ui.UI.executeLockedly(ctx.getWorkbench().getRegistry().getProjectGraph(), true, 
				CONVERT , CloudGraph.class, ctx, JobManager.ACTION_FLAGS);
	}

	static Command CONVERT = new Command() {
		@Override public String getCommandName() { return null; }
		@Override
		public void run(Object info, Context context) {
			if (!(info instanceof Class)) { return; }
			List<Node> objects = getSelectedNodes(context);
			List<Cloud> clouds = new ArrayList<Cloud>();
			if(objects.size() < 1)
				return;
			for(int i = 0; i < objects.size(); i++) {
				if(objects.get(i) instanceof CloudContext) {
					clouds.add(((CloudContext)objects.get(i)).getCloud());
				}
			}
			for (Cloud c : clouds) {
				Node n = c.getNode();
				if (!(n instanceof PointCloud)) { continue; }
				((PointCloud)n).setCloud(Tools.convert(c, (Class)info));
			}
		}
	};

	
	//	/**
	//	 * Generates / Fits a sphere for each point cloud in the current 3d view
	//	 * selection and adds the spheres to the 3d view. If the selection does not
	//	 * fit, an error dialog is shown. The point clouds can optionally be
	//	 * removed.
	//	 *
	//	 * @param item The item that was clicked
	//	 * @param information Information about the clicked menu entry
	//	 * @param context The context where the button was clicked
	//	 */
	//	public static void fitSpheresToPointClouds(Item item, Object information, Context context) {
	//		List<Node> selected = getSelectedNodes(context);
	//		List<Cloud> clouds = new ArrayList<Cloud>();
	//		for (Node n : selected) {
	//			if (n instanceof CloudContext) {
	//				clouds.add(((CloudContext)n).getCloud());
	//			}
	//		}
	//		if (clouds.isEmpty()) { return; }
	//		
	//		PointCloud[] array = new PointCloud[clouds.size()];
	//		int i = 0;
	//		for (Cloud cloud : clouds) {
	//			if (cloud.getNode() instanceof PointCloud) {
	//				array[i] = (PointCloud) cloud.getNode();
	//				i++;
	//			}
	//		}
	//		
	//		List<String> labels = List.of("Precision", "Use average fitting");
	//        List<Class<?>> types = List.of(Integer.class, Boolean.class);
	//        
	//        JPanel panel = UIDialog.createDynamicInputPanel(labels, types);
	//        String title = "Sphere Fitting";
	//		int buttons = JOptionPane.OK_CANCEL_OPTION;
	//		int type = JOptionPane.QUESTION_MESSAGE;
	//		int result = UIDialog.getInstance().displayParameterInput(panel, title, buttons, type);
	//		if(result == JOptionPane.OK_OPTION) {
	//			List<Object> inputs = UIDialog.getInputValues(panel, types);
	//			int precision = (int) inputs.get(0);
	//			boolean avg = (boolean) inputs.get(1);
	//
	//			de.grogra.imp3d.objects.Sphere[] spheres = new de.grogra.imp3d.objects.Sphere[array.length];
	//			i=0;
	//			for (PointCloud pc : array) {
	//				if (!(pc instanceof CloudList)) { continue; } // only cloud list supported for now
	//				spheres[i] = ShapeFitting.fitSphereToPointCloud((CloudList)pc, avg);
	//				IMP.addNode(null, spheres[i], context);
	//				i++;
	//			}
	//			
	//		}
	//	}
	//	
	//	/**
	//	 * Generates / Fits a cylinder for each point cloud in the current 3d view
	//	 * selection and adds the cylinders to the 3d view. If the selection does
	//	 * not fit, an error dialog is shown. The point clouds can optionally be
	//	 * removed.
	//	 *
	//	 * @param item The item that was clicked
	//	 * @param information Information about the clicked menu entry
	//	 * @param context The context where the button was clicked
	//	 */
	//	public static void fitCylindersToPointClouds(Item item, Object information, Context context) {
	//		List<Node> selected = getSelectedNodes(context);
	//		List<Cloud> clouds = new ArrayList<Cloud>();
	//		for (Node n : selected) {
	//			if (n instanceof CloudContext) {
	//				clouds.add(((CloudContext)n).getCloud());
	//			}
	//		}
	//		if (clouds.isEmpty()) { return; }
	//		
	//		PointCloud[] array = new PointCloud[clouds.size()];
	//		int i = 0;
	//		for (Cloud cloud : clouds) {
	//			if (cloud.getNode() instanceof PointCloud) {
	//				array[i] = (PointCloud) cloud.getNode();
	//				i++;
	//			}
	//		}
	//		
	//		List<String> labels = List.of("Precision", "Use average fitting");
	//        List<Class<?>> types = List.of(Integer.class, Boolean.class);
	//        
	//        JPanel panel = UIDialog.createDynamicInputPanel(labels, types);
	//        String title = "Sphere Fitting";
	//		int buttons = JOptionPane.OK_CANCEL_OPTION;
	//		int type = JOptionPane.QUESTION_MESSAGE;
	//		int result = UIDialog.getInstance().displayParameterInput(panel, title, buttons, type);
	//		if(result == JOptionPane.OK_OPTION) {
	//			List<Object> inputs = UIDialog.getInputValues(panel, types);
	//			int precision = (int) inputs.get(0);
	//			boolean avg = (boolean) inputs.get(1);
	//
	//			de.grogra.imp3d.objects.Cylinder[] cyl = new de.grogra.imp3d.objects.Cylinder[array.length];
	//			i=0;
	//			for (PointCloud pc : array) {
	//				if (!(pc instanceof CloudList)) { continue; } // only cloud list supported for now
	//				cyl[i] = ShapeFitting.fitCylinderToPointCloud((CloudList)pc, avg, precision);
	//				IMP.addNode(null, cyl[i], context);
	//				i++;
	//			}
	//		}
	//	}
}
