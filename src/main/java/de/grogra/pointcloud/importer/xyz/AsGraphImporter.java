package de.grogra.pointcloud.importer.xyz;

import java.io.File;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.mesh.renderer.handler.CollectionDisplayable;
import de.grogra.mesh.renderer.handler.CollectionPointHandler;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.registry.Item;
import de.grogra.pointcloud.importer.PointCloudFilterBase;
import de.grogra.pointcloud.objects.IntermediateCloudNode;
import de.grogra.pointcloud.objects.impl.CloudGraph;
import de.grogra.pointcloud.objects.impl.CollectionCloudImpl;
import de.grogra.pointcloud.objects.impl.IntermediateCloudNodeImpl;
import de.grogra.pointcloud.objects.impl.LeafPointImpl;
import de.grogra.util.ProgressMonitor;
import de.grogra.util.Utils;


public class AsGraphImporter {

	public static class LoaderAsNode extends PointCloudFilterBase implements ObjectSource
	{
		public LoaderAsNode (FilterItem item, FilterSource source)
		{
			super (item, source);
			setFlavor (IOFlavor.NODE);
		}

		public Object getObject () throws IOException {
			int intermediateNodeBatchSize = getOptionMaxPoint();
			FileChannel fileChannel = FileChannel.open( 
					((File)((FileSource)source).getFile()).toPath(), StandardOpenOption.READ);
			int numberOfThreads = Runtime.getRuntime().availableProcessors() -2 ;
//			int numberOfThreads = 2;
			long chunkSize = fileChannel.size() / numberOfThreads;
			source.setProgress("Reading the chunks", -1);
			List<AddLeavesThread> threads = new ArrayList<>();
			long start = 0;
			long stop;
			long missing = fileChannel.size();
			MappedByteBuffer buffer;
			
			List<IntermediateCloudNode> intermediateNodes = new ArrayList<IntermediateCloudNode>(); 

			for (int i = 0; i < numberOfThreads; i++) {
				if (missing > chunkSize) {
					buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, chunkSize);
					stop = chunkSize - estimateNextLine(buffer);
				} else {
					stop = missing;
				}
				buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, stop);
				missing -= stop;
				stop += start;
				start = stop;

				AddLeavesThread counterThread = new AddLeavesThread(buffer, intermediateNodeBatchSize);
				threads.add(counterThread);
				counterThread.start();

			}
			// last chunk
			buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, missing);
			AddLeavesThread counterThread = new AddLeavesThread(buffer, intermediateNodeBatchSize);
			threads.add(counterThread);
			counterThread.start();

			try {
				for (AddLeavesThread thread : threads) {
					thread.join();
					intermediateNodes.addAll( thread.getResult());
					thread.clear();
				}
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			
			pcRoot=new CollectionCloudImpl();
			Class cls;
			try {
				cls = Class.forName("de.grogra.pointcloud.objects.impl.LeafPointImpl");
			} catch(ClassNotFoundException e) {
				cls = null;
			}
			long depth = CloudGraph.computeDepthFromNandD(intermediateNodes.size(), intermediateNodeBatchSize);

			CloudGraph pc = new CloudGraph(source.getSystemId(), intermediateNodeBatchSize, depth, cls);
			int l = 0;
			for (IntermediateCloudNode c : intermediateNodes) {
				pc.addPoint(c);
				source.setProgress("Merging the points : " + l + "/" + intermediateNodes.size(), 
						(float) l/(float)intermediateNodes.size());
				l++;
			}
			CollectionPointHandler cm = new CollectionPointHandler();
			if (pc instanceof CollectionDisplayable) { ((CollectionDisplayable)pc).setDisplayHandler(cm); }
//			cm.addEdgeBitsTo(pc ,  Graph.BRANCH_EDGE, null);
			pcRoot.addEdgeBitsTo(pc.getNode(), Graph.BRANCH_EDGE, null);
			setProgress("done", ProgressMonitor.DONE_PROGRESS);

			return pcRoot;
		}

		/**
		 * Used for debug
		 */
		private String dump(MappedByteBuffer buffer) {
			StringBuffer s = new StringBuffer();
			while (buffer.hasRemaining()) {
				s.append((char) buffer.get());
			}
			return s.toString();
		}

		public Node getPoint(long id) {
			return null;
		}

		public Node[] getPoints(long[] list) {
			return null;
		}

		public Node[] getPoints(int[] list) {
			return null;
		}

		public Node[] getPoints(Object list) {
			return null;
		}


		@Override
		public void endExport() {
			allPoints.clear();
		}

		@Override
		public String getKey(Object o) {
			return null;
		}
	}
	
	static private int getOptionMaxPoint() {
		Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
		return (int) Utils.get(ite, "maxpointpernode", 1000);
	}

	static private boolean getOptionVisible() {
		Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
		return (boolean) Utils.get(ite, "visible", true);
	}

	
	static private int estimateNextLine(MappedByteBuffer buf) {
		int i = buf.limit()-1;
		int j = 0;
		while (i>0) {
			if(buf.get(i) == '\n') {
				return j;
			}
			j++;
			i--;
		}
		return 0;
	}

	static class AddLeavesThread extends Thread {

		List<IntermediateCloudNode> result = new ArrayList<IntermediateCloudNode>();
		private int maxCapacityForInternode;
		private MappedByteBuffer buffer;

		public AddLeavesThread(MappedByteBuffer buffer, int maxCapacityForInternode) {
			this.buffer = buffer;
			this.maxCapacityForInternode = maxCapacityForInternode;
			
//			System.out.println("creation of runner to look at : ");
//			System.err.println(dump(buffer));
			
		}

		@Override
		public void run() {
			int i = 0;
			double[] pos = new double[3];
			int j = 0; // j goes from 0 to 2 for x, y, z
			StringBuffer number = new StringBuffer();
			boolean commentLine = false;
			boolean numberComplete = false; // true when a non number char is read (i.e. the buffer contains a number)
			boolean lineComplete = false; // true after 3 number read on a line. The other char are additional non supported info
			IntermediateCloudNodeImpl inter = new IntermediateCloudNodeImpl();
			while (buffer.hasRemaining()) {					
				if (i >= maxCapacityForInternode) {
					result.add(inter);
					i = 0;
					inter = new IntermediateCloudNodeImpl();
				}

				char c = (char) buffer.get();
				
				if (commentLine) {
					if (c == '\n') { commentLine= false; }
					else { continue; }
				}
				if (c == '#') { commentLine = true; continue; }
				if (!(c == ' ' || c == ',' || c == ';' || c == '\t' || c == '\r' || c == '\n')) {
					number.append(c);
				} else {
					numberComplete = true;
				}
				if (numberComplete && ! lineComplete) {
					numberComplete = false;
					String val = number.toString();
					if (!val.isEmpty()) {
						number.setLength(0);
						pos[j%3] = Double.valueOf(val);
						j++;
					}
					if (j == 3) {
						j=0;
						lineComplete = true;
						inter.addEdgeBitsTo(new LeafPointImpl(pos), Graph.BRANCH_EDGE, null);
						i++;
					}
				}
				if (lineComplete) {
					if (c == '\n') { 
						lineComplete= false;
						numberComplete = false;
						number.setLength(0);
					}
				}
			}
			// add the last intermediate node
			result.add(inter);
			
		}

		public List<IntermediateCloudNode> getResult(){
			return result;
		}
		public void clear() {
			result.clear();
		}
	}
	
	public static Node doImport(File in) throws IOException {
		int intermediateNodeBatchSize = getOptionMaxPoint();
		FileChannel fileChannel = FileChannel.open( in.toPath(), StandardOpenOption.READ);
		int numberOfThreads = Runtime.getRuntime().availableProcessors() -2 ;
//		int numberOfThreads = 2;
		long chunkSize = fileChannel.size() / numberOfThreads;
		List<AddLeavesThread> threads = new ArrayList<>();
		long start = 0;
		long stop;
		long missing = fileChannel.size();
		MappedByteBuffer buffer;
		
		List<IntermediateCloudNode> intermediateNodes = new ArrayList<IntermediateCloudNode>(); 

		for (int i = 0; i < numberOfThreads; i++) {
			if (missing > chunkSize) {
				buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, chunkSize);
				stop = chunkSize - estimateNextLine(buffer);
			} else {
				stop = missing;
			}
			buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, stop);
			missing -= stop;
			stop += start;
			start = stop;

			AddLeavesThread counterThread = new AddLeavesThread(buffer, intermediateNodeBatchSize);
			threads.add(counterThread);
			counterThread.start();

		}
		// last chunk
		buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, missing);
		AddLeavesThread counterThread = new AddLeavesThread(buffer, intermediateNodeBatchSize);
		threads.add(counterThread);
		counterThread.start();

		try {
			for (AddLeavesThread thread : threads) {
				thread.join();
				intermediateNodes.addAll( thread.getResult());
				thread.clear();
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		
		Node pcRoot=new CollectionCloudImpl();
		Class cls;
		try {
			cls = Class.forName("de.grogra.pointcloud.objects.impl.LeafPointImpl");
		} catch(ClassNotFoundException e) {
			cls = null;
		}
		long depth = CloudGraph.computeDepthFromNandD(intermediateNodes.size(), intermediateNodeBatchSize);

		CloudGraph pc = new CloudGraph(in.getName(), intermediateNodeBatchSize, depth, cls);
		int l = 0;
		for (IntermediateCloudNode c : intermediateNodes) {
			pc.addPoint(c);
			l++;
		}
		CollectionPointHandler cm = new CollectionPointHandler();
		if (pc instanceof CollectionDisplayable) { ((CollectionDisplayable)pc).setDisplayHandler(cm); }
//		cm.addEdgeBitsTo(pc ,  Graph.BRANCH_EDGE, null);
		pcRoot.addEdgeBitsTo(pc.getNode(), Graph.BRANCH_EDGE, null);

		return pcRoot;
	}

}
