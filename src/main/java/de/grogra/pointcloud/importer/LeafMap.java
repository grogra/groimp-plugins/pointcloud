package de.grogra.pointcloud.importer;

import java.util.HashMap;

import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ObjectItem;

/**
 * Read the registry and map the name from the items bellow the filter 
 * to their given class.
 */
public class LeafMap implements de.grogra.util.Map{

	private HashMap table = new HashMap ();

	private final Item base;

	public LeafMap (Item base)
	{
		base.getClass ();
		this.base = base;
	}
	
	@Override
	public Object get (Object key, Object defaultValue)
	{
		key = eval ((key instanceof String) ? (String) key : key.toString());
		return (key != null) ? key : defaultValue;
	}
	
	public synchronized Object eval (String name)
	{
		Object o = table.get (name);
		if (o == null)
		{
			Item f = base.getItem (name);
			if (f instanceof ObjectItem)
			{
				o = ((ObjectItem) f).getObject ();
			}
			table.put (name, (o == null) ? this : o);
		}
		return (o == this) ? null : o;
	}
}
