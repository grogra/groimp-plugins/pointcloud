package de.grogra.pointcloud.importer.ply;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.smurn.jply.Element;
import org.smurn.jply.ElementReader;
import org.smurn.jply.ElementType;
import org.smurn.jply.ListProperty;
import org.smurn.jply.PlyReaderFile;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.ArrayPoint;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.mesh.renderer.handler.FloatArrayHandler;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.InputStreamSource;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.registry.Item;
import de.grogra.pointcloud.importer.PointCloudFilterBase;
import de.grogra.pointcloud.objects.impl.CloudList;
import de.grogra.pointcloud.objects.impl.CollectionCloudImpl;
import de.grogra.pointcloud.objects.impl.LeafLineImpl;
import de.grogra.pointcloud.objects.impl.LeafMeshImpl;
import de.grogra.pointcloud.objects.impl.LeafPointImpl;
import de.grogra.pointcloud.objects.impl.PointCloudD;
import de.grogra.util.ProgressMonitor;
import de.grogra.util.Utils;


public class AsListImporter {

	public static class LoaderAsNode extends PointCloudFilterBase implements ObjectSource
	{
		public LoaderAsNode (FilterItem item, FilterSource source)
		{
			super (item, source);
			setFlavor (IOFlavor.NODE);
		}

		public Object getObject () throws IOException
		{
			InputStream in = ((InputStreamSource) source).getInputStream ();
			PlyReaderFile plyreader = new PlyReaderFile(in);

			pcRoot =new CollectionCloudImpl();
			Node n=null;

			Cloud outputCloud = new CloudList();

			ElementReader reader = plyreader.nextElementReader();
			while (reader != null) {
				if (reader.getElementType().getName().contentEquals("vertex")) {
					readPointToCloud(reader, outputCloud);
				}
				reader.close();
				reader = plyreader.nextElementReader();
			}
			plyreader.close();
			endExport();

			pcRoot = new PointCloudD(outputCloud);
			((PointCloudD)pcRoot).setDisplayHandler(new FloatArrayHandler());
			setProgress("done", ProgressMonitor.DONE_PROGRESS);
			
			return pcRoot;
		}


		private void readPointToCloud(ElementReader reader, Cloud cloud) throws IOException {
			ElementType e = reader.getElementType();
			Element leaf = reader.readElement();

			int i = 1;
			int j = 0; // 0 to 3
			int totalsize = reader.getCount();

			while (leaf !=null) {	
				j=0;
				float[] pos = new float[3];

				for (org.smurn.jply.Property p : e.getProperties()) {
					float val=0;
					List<Float> vals = new ArrayList<Float>();

					if (p instanceof ListProperty) { //SHOULD NEVER HAPPENS FOR VERTEX X,Y,Z
						switch(p.getType()) {
						case CHAR:
						case UCHAR:
						case SHORT:
						case USHORT:
						case INT:
						case UINT:
							for (int t :  leaf.getIntList(p.getName())) {
								vals.add((float)t);
							}
							break;
						case FLOAT:
						case DOUBLE:
							for (double t :  leaf.getDoubleList(p.getName())) {
								vals.add((float)t);
							}
							break;
						}											

					} else {
						switch(p.getType()) {
						case CHAR:
						case UCHAR:
						case SHORT:
						case USHORT:
						case INT:
						case UINT:
							val = ((Integer) leaf.getInt(p.getName())).floatValue();
							break;
						case FLOAT:
						case DOUBLE:
							val = ((Double) leaf.getDouble(p.getName())).floatValue();
							break;
						}					
					}
					if (j<3) {
						pos[j] = val;
					}
					j++;
				}

				cloud.addPoint(new ArrayPoint( pos));
				i++;
				source.setProgress("Reading " + e.getName()+" : "+i+" /"+totalsize, (float) i/(float)totalsize);
				leaf = reader.readElement();
			}
		}


		private int getOptionMaxPoint() {
			Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
			return (int) Utils.get(ite, "maxpointpernode", 1000);
		}

		private boolean getOptionVisible() {
			Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
			return (boolean) Utils.get(ite, "visible", true);
		}

		public Node getPoint(long id) {
			return allPoints.get(id);
		}

		public Node[] getPoints(long[] list) {
			List<Node> tmp = new ArrayList<Node>();
			for (long id: list) {
				tmp.add(allPoints.get(id));
			}
			return tmp.toArray(new Node[0]);
		}

		public Node[] getPoints(int[] list) {
			long[] ids = Arrays.stream(list).asLongStream().toArray();
			return getPoints(ids);
		}

		public Node[] getPoints(Object list) {
			if (list instanceof int[]) {
				return getPoints((int[])list);
			}
			if (list instanceof long[]) {
				return getPoints((long[])list);
			}
			return null;
		}

		public String getKey(Object key) {
			if (key instanceof LeafPointImpl.Keys) {
				switch((LeafPointImpl.Keys)key) {
				case X:
					return "x";
				case Y:
					return "y";
				case Z:
					return "z";
				case RED:
					return "red";
				case GREEN:
					return "green";
				case BLUE:
					return "blue";
				case ALPHA:
					return "alpha";
				default:
					return "";
				}
			}
			if (key instanceof LeafLineImpl.Keys) {
				switch((LeafLineImpl.Keys)key) {
				case VERTEX1:
					return "vertex1";
				case VERTEX2:
					return "vertex2";
				case RED:
					return "red";
				case GREEN:
					return "green";
				case BLUE:
					return "blue";
				case ALPHA:
					return "alpha";
				default:
					return "";
				}
			}
			if (key instanceof LeafMeshImpl.Keys) {
				switch((LeafMeshImpl.Keys)key) {
				case VERTEX_IDS:
					return "vertex_indices";
				case RED:
					return "red";
				case GREEN:
					return "green";
				case BLUE:
					return "blue";
				case ALPHA:
					return "alpha";
				default:
					return "";
				}
			}
			return "";
		}

		@Override
		public void endExport() {
			allPoints.clear();
		}
	}
}
