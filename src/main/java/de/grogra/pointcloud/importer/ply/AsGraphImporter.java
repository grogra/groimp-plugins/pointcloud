package de.grogra.pointcloud.importer.ply;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.smurn.jply.Element;
import org.smurn.jply.ElementReader;
import org.smurn.jply.ElementType;
import org.smurn.jply.ListProperty;
import org.smurn.jply.PlyReaderFile;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.mesh.renderer.handler.CollectionDisplayHandler;
import de.grogra.mesh.renderer.handler.CollectionDisplayable;
import de.grogra.mesh.renderer.handler.CollectionMeshHandler;
import de.grogra.mesh.renderer.handler.CollectionPointHandler;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.InputStreamSource;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.registry.Item;
import de.grogra.pointcloud.importer.PointCloudFilterBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.pointcloud.objects.impl.CloudGraph;
import de.grogra.pointcloud.objects.impl.CollectionCloudImpl;
import de.grogra.pointcloud.objects.impl.LeafLineImpl;
import de.grogra.pointcloud.objects.impl.LeafMeshImpl;
import de.grogra.pointcloud.objects.impl.LeafPointImpl;
import de.grogra.util.ProgressMonitor;
import de.grogra.util.StringMap;
import de.grogra.util.Utils;
import de.grogra.xl.util.XHashMap;


public class AsGraphImporter {
				
	public static class LoaderAsNode extends PointCloudFilterBase implements ObjectSource
		{
		public LoaderAsNode (FilterItem item, FilterSource source)
		{
			super (item, source);
			setFlavor (IOFlavor.NODE);
		}
		
		public Object getObject () throws IOException
		{
			InputStream in = ((InputStreamSource) source).getInputStream ();
			PlyReaderFile plyreader = new PlyReaderFile(in);
						
	        pcRoot=new CollectionCloudImpl();
	        Node n=null;

	        // if there are several elements in the ply. It is expected that there is 
	        // AT least the vertices, and if > 1, the vertices are added to a map for latter mapping
	        addPoints = (plyreader.getElementTypes().size()>1)?true:false;
	        //readFromMap is true if addPoints is true AND the map has been filled already (i.e. 
	        // after the points were loaded and added to it.
	        boolean readFromMap=false;
	        ElementReader reader = plyreader.nextElementReader();
	        while (reader != null) {
	        	if (addPoints && !allPoints.isEmpty()) {
	        		readFromMap=true;
	        	}
	        	CollectionDisplayHandler cm;
	        	Node cloud = readerToNodes(reader, readFromMap);
	        	if (!readFromMap) { cm = new CollectionPointHandler(); }
	        	else { cm = new CollectionMeshHandler(); }
	        	if (cloud instanceof CollectionDisplayable) { ((CollectionDisplayable)cloud).setDisplayHandler(cm); }
//	        	cm.addEdgeBitsTo(readerToNodes(reader, readFromMap),  Graph.BRANCH_EDGE, null);
        		pcRoot.addEdgeBitsTo(cloud, Graph.BRANCH_EDGE, null);

	        	reader.close();
	        	reader = plyreader.nextElementReader();
	        }
			
			plyreader.close();
			endExport();
			setProgress("done", ProgressMonitor.DONE_PROGRESS);

			return pcRoot;
		}
		
		
		private Node readerToNodes(ElementReader reader, boolean readFromMap) throws IOException {
			ElementType e = reader.getElementType();
			Element leaf = reader.readElement();
			Class cls = leaves.eval(e.getName()).getClass();
			
			int i = 1;
			int batchSize = getOptionMaxPoint();
			int totalsize = reader.getCount();
			long depth = CloudGraph.computeDepthFromNandD(totalsize, batchSize);
			CloudGraph pc = new CloudGraph(source.getSystemId(), batchSize, depth, cls);

			if (addPoints && !readFromMap) {
				allPoints = new XHashMap<Long, Node>(totalsize, 1f);
			}
			
			while (leaf !=null) {	
				StringMap args = new StringMap();
				args.put("ctx", this);
				for (org.smurn.jply.Property p : e.getProperties()) {
					Object val=null;

					if (p instanceof ListProperty) {
						switch(p.getType()) {
						case CHAR:
						case UCHAR:
					    case SHORT:
					    case USHORT:
					    case INT:
					    case UINT:
					    	val = leaf.getIntList(p.getName());
					    	break;
					    case FLOAT:
					    case DOUBLE:
					    	val = leaf.getDoubleList(p.getName());
					    	break;
						}											
						args.put(p.getName(), val);

					}else {
						switch(p.getType()) {
						case CHAR:
						case UCHAR:
					    case SHORT:
					    case USHORT:
					    case INT:
					    case UINT:
					    	val = leaf.getInt(p.getName());
					    	break;
					    case FLOAT:
					    case DOUBLE:
					    	val = leaf.getDouble(p.getName());
					    	break;
						}					
						args.put(p.getName(), val);
					}
				}

				PointCloudLeaf o = pc.createLeaf(new Object[] {args});
				o.setIdImport(i-1);
				pc.addPoint(o);
				
				if (addPoints && !readFromMap) {
					allPoints.add((long)i-1, o.getNode());
				}
				i++;
				source.setProgress("Reading " + e.getName()+" : "+i+" /"+totalsize, (float) i/(float)totalsize);
				leaf = reader.readElement();
			}
			return pc.getNode();
		}

	
		private int getOptionMaxPoint() {
			Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
			return (int) Utils.get(ite, "maxpointpernode", 1000);
		}
		
		private boolean getOptionVisible() {
			Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
			return (boolean) Utils.get(ite, "visible", true);
		}
		
		public Node getPoint(long id) {
			return allPoints.get(id);
		}
		
		public Node[] getPoints(long[] list) {
			List<Node> tmp = new ArrayList<Node>();
			for (long id: list) {
				tmp.add(allPoints.get(id));
			}
			return tmp.toArray(new Node[0]);
		}
		
		public Node[] getPoints(int[] list) {
			long[] ids = Arrays.stream(list).asLongStream().toArray();
			return getPoints(ids);
		}
		
		public Node[] getPoints(Object list) {
			if (list instanceof int[]) {
				return getPoints((int[])list);
			}
			if (list instanceof long[]) {
				return getPoints((long[])list);
			}
			return null;
		}
		
		public String getKey(Object key) {
			if (key instanceof LeafPointImpl.Keys) {
				switch((LeafPointImpl.Keys)key) {
				case X:
					return "x";
				case Y:
					return "y";
				case Z:
					return "z";
				case RED:
					return "red";
				case GREEN:
					return "green";
				case BLUE:
					return "blue";
				case ALPHA:
					return "alpha";
				default:
					return "";
				}
			}
			if (key instanceof LeafLineImpl.Keys) {
				switch((LeafLineImpl.Keys)key) {
				case VERTEX1:
					return "vertex1";
				case VERTEX2:
					return "vertex2";
				case RED:
					return "red";
				case GREEN:
					return "green";
				case BLUE:
					return "blue";
				case ALPHA:
					return "alpha";
				default:
					return "";
				}
			}
			if (key instanceof LeafMeshImpl.Keys) {
				switch((LeafMeshImpl.Keys)key) {
				case VERTEX_IDS:
					return "vertex_indices";
				case RED:
					return "red";
				case GREEN:
					return "green";
				case BLUE:
					return "blue";
				case ALPHA:
					return "alpha";
				default:
					return "";
				}
			}
			return "";
		}

		@Override
		public void endExport() {
			allPoints.clear();
		}
	}
		
}
